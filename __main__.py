
import requests
import json
import ctfapi


data = ctfapi.ctfapi()

chal = data.get("/api/v1/challenges").json()['data']
tasks = [ _['name'] for _ in chal ]
score = data.get("/api/v1/scoreboard").json()['data']
teams = data.get("/api/v1/teams").json()['data']

standings=[ {"pos": _['pos'] ,
             "team": _['name'],
             "score": _['score']} for _ in score ]

CtfTimeScore={
    "tasks": tasks,
    "standings": standings
    }

def getTeamId( id ):
    global teams
    return [ _ for _ in teams if _['name'] == id ][0]

def getSolves( id):
   global data
   solves= data.get(f"/api/v1/teams/{id}/solves").json()['data']
   taskStats={}
   for _ in solves:
      taskStats[_['challenge']['name']]= {"points": _['challenge']['value']}
   return taskStats

for _ in CtfTimeScore['standings']:
   print(f"Get Score for team {_['team']}")
   id = getTeamId(_['team'])['id'] 
   _['taskStats']=getSolves(id)
   print(f"`---> { len(_['taskStats'])} Challenges {_['score']} ")  


with open("score.json",'w') as sc:
   sc.write( json.dumps(CtfTimeScore,indent=4))
