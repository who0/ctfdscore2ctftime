

import os
import requests
import json
import yaml

url = os.environ.get("URL")
API = os.environ.get('API')


class ctfapi(requests.sessions.Session):
    baseurl=os.environ.get("URL")


    def __init__(self):
        requests.sessions.Session.__init__(self)
        self.headers.update({"Authorization": f"Token {API}"})
        r=self.get(f"/api/v1/tokens",headers={'Content-type':'application/json'}).json()
        if (not r['success']):
            print("No token or no env")
            exit()
        return

    def request(self, method, url, *args, **kwargs):
        url=f"{self.baseurl}{url}"
        #print(url)
        return super().request(method, url, *args, **kwargs)

    def getPos(self,num=1):
       try:
         return [ _ for _ in self.get("/api/v1/scoreboard").json()['data'] if _['pos']==num ][0]
       except:
         return []

    def getUser(self,id):
       return self.get(f"/api/v1/users/{id}").json()['data']


    def config(self):
       c=self.get("/api/v1/configs").json()
       with  open("config.yml","w") as f:
            yaml.dump(c,f)
       return True
        
    def score(self):
      return self.get("/api/v1/scoreboard").json()['data']
