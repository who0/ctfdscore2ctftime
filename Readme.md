# Ctfd > Ctftime Json score

Convert ctfd scoreboard to ctftime.json format.

Set Environement variable
```bash
export URL=<CTFDServer>
export API=<CTFDToken>
```

```bash
python3 .
```

"score.json" could be uploaded to ctftime including 
 * **score** 
 * **pos** 
 * **teams** 
 * **tasks**

exaample format from https://ctftime.org/json-scoreboard-feed
```json
{
	"tasks": [ "PPC 100", "Web 200", "Beer Challenge" ],
	"standings": [
		{ "pos": 1, "team": "Intergalactic Pwners", "score": 401, 
			"taskStats": {
				"PPC 100": { "points": 103, "time": 1352647482 },
				"Web 200": { "points": 201, "time": 1352652183 },
				"Beer Challenge": { "points": 97, "time": 1352651383 }
			}, 
			"lastAccept": 1352652183 },
		{ "pos": 2, "team": "h4ckmeifyouc4n", "score": 291, 
			"taskStats": { 
				"Web 200" : { "points": 202, "time": 1352649284 },
				"Beer Challenge": { "points": 89, "time": 1352649511 }
			}, 
			"lastAccept": 1352649511 },
		{ "pos": 3, "team": "MV Tech", "score": 203, 
			"taskStats": { 
				"Web 200": { "points": 203, "time": 1352650418 }
			}, 
			"lastAccept": 1352650418 }
	]
}
```